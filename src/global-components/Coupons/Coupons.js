import React from 'react';
import { Card } from 'react-bootstrap';
import './Coupons.css';

export class Coupons extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className='coupon'>
                <div className='sub-coupon-1 coupon-rounded'>
                    <div>coupon<br />text</div>
                </div>

                <div className = 'sub-coupon-2 coupon-thumbnail'>
                    <div>Pedicure<br/> coupon </div>
                </div>

                <div className='sub-coupon-3 coupon-rounded'>
                    <div>coupon<br />text</div>
                </div>
            </div>
        );
    }
};