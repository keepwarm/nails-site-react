import React from 'react';
import { StaffCardComponent } from '../StaffCardComponent/StaffCardComponent';
import { CardGroup } from 'react-bootstrap';

export class StaffCardGroupComponent extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <CardGroup>
                {this.props.staffData.map((data) => (
                    <StaffCardComponent image={data.image} name={data.name} description={data.description} />

                ))};
            </CardGroup>
        )
    }
}   