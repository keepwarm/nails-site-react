import React from 'react';
import {Navbar, NavDropdown, Col, Nav, Container, Row, Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import logo from '../../media/HarlieNailsLogo.png';

export class NavigationComponent extends React.Component{
    // constructor(props){
    //     super(props);
    // }
   
    render(){
        return (
//             <nav class="navbar navbar-expand-md navbar-fixed-top navbar-dark bg-dark main-nav">
//     <div class="container">
//         <div class="navbar-collapse collapse nav-content order-2">
//             <ul class="nav navbar-nav">
//                 <li class="nav-item active">
//                     <a class="nav-link" href="#">Home</a>
//                 </li>
//                 <li class="nav-item">
//                     <a class="nav-link" href="#">Download</a>
//                 </li>
//                 <li class="nav-item">
//                     <a class="nav-link" href="#">Register</a>
//                 </li>
//             </ul>
//         </div>
//         <ul class="nav navbar-nav text-nowrap flex-row mx-md-auto order-1 order-md-2">
//             <li class="nav-item"><a class="nav-link" href="#">Website Name</a></li>
//             <button class="navbar-toggler ml-2" type="button" data-toggle="collapse" data-target=".nav-content" aria-expanded="false" aria-label="Toggle navigation">
//                 <span class="navbar-toggler-icon"></span>
//             </button>
//         </ul>
//         <div class="ml-auto navbar-collapse collapse nav-content order-3 order-md-3">
//             <ul class="ml-auto nav navbar-nav">
//                 <li class="nav-item">
//                     <a class="nav-link" href="#">Rates</a>
//                 </li>
//                 <li class="nav-item">
//                     <a class="nav-link" href="#">Help</a>
//                 </li>
//                 <li class="nav-item">
//                     <a class="nav-link" href="#">Contact</a>
//                 </li>
//             </ul>
//         </div>
//     </div>
// </nav>
            <Navbar navbar-expand-md navbar-fixed-top navbar-dark>
                <Container>
                {/* <Navbar.Toggle aria-controls='basic-navbar-nav' /> */}
                            <Nav className='navbar-collapse collapse nav-content order-2'>
                                <Nav.Link className='nav navbar-nav' href='/'>Home</Nav.Link>
                                <Nav.Link className='nav navbar-nav' href='/about'>About</Nav.Link>
                                <NavDropdown title='Services' id='basic-nav-dropdown'>
                                    <NavDropdown.Item className="nav-item active" href='/ped'>Pedicure</NavDropdown.Item>
                                    <NavDropdown.Item className="nav-item active" href='/mani'>Manicure</NavDropdown.Item>
                                    <NavDropdown.Item className="nav-item active" href='/change'>Polish Change</NavDropdown.Item>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item href='/other'>Other Services</NavDropdown.Item>
                                </NavDropdown>
                            </Nav>

                            <Nav className = 'nav navbar-nav text-nowrap flex-row mx-md-auto order-1 order-md-2'> 
                            <Navbar.Brand className="nav-item"> 
                                <a class="nav-link" href='/'>
                                <img src = {logo} height='80' width='120' alt='Harlie Nails'/>
                                </a>
                                {/* Harlie Nails */}
                             </Navbar.Brand>
                            </Nav> 

                            <Nav className='ml-auto navbar-collapse collapse nav-content order-3 order-md-3'>
                                <NavDropdown className = 'ml-auto nav navbar-nav' title='Products' id='basic-nav-dropdown'>
                                    <NavDropdown.Item className="nav-item active" href='/dip'>Dip Powder</NavDropdown.Item>
                                    <NavDropdown.Item className="nav-item active" href='/gel'>Gel Polish</NavDropdown.Item>
                                    <NavDropdown.Item className="nav-item active" href='/regular'>Regular Polish</NavDropdown.Item>
                                </NavDropdown>
                                <Nav.Link className="nav-item" href='/appointment'>Appointment</Nav.Link>
                                <Nav.Link className="nav-item" href='/contact'>Contacts</Nav.Link>
                            </Nav>
                </Container>
            </Navbar>

            // <Navbar bg='ligth' expand='lg'>
            //     <Navbar.Toggle aria-controls='basic-navbar-nav' />
            //     <Navbar id='basic-navbar-nav'>
            //         <Container>
            //             <Row>
            //                 <Col>
            //                     <Nav className='mr-auto'>
            //                         <Nav.Link href='/'>Home</Nav.Link>
            //                         <Nav.Link href='/about'>About</Nav.Link>
            //                         <NavDropdown title='Services' id='basic-nav-dropdown'>
            //                             <NavDropdown.Item href='/ped'>Pedicure</NavDropdown.Item>
            //                             <NavDropdown.Item href='/mani'>Manicure</NavDropdown.Item>
            //                             <NavDropdown.Item href='/change'>Polish Change</NavDropdown.Item>
            //                             <NavDropdown.Divider />
            //                             <NavDropdown.Item href='/other'>Other Services</NavDropdown.Item>
            //                         </NavDropdown>
            //                     </Nav>
            //                 </Col>
            //                 <Col>
            //                     <Navbar.Brand href='/'>
            //                         <img
            //                             alt=''
            //                             src='./media/HarlieNailsLogo.png'
            //                             width='30'
            //                             height='30'
            //                             className='d-inline-block align-top'
            //                         /> {' '}
            //       Harlie Nails
            //     </Navbar.Brand>
            //                 </Col>
            //                 <Col>
            //                     <Nav className='mr-auto'>
            //                         <NavDropdown title='Products' id='basic-nav-dropdown'>
            //                             <NavDropdown.Item href='/dip'>Dip Powder</NavDropdown.Item>
            //                             <NavDropdown.Item href='/gel'>Gel Polish</NavDropdown.Item>
            //                             <NavDropdown.Item href='/regular'>Regular Polish</NavDropdown.Item>
            //                         </NavDropdown>
            //                         <Nav.Link href='/appointment'>Appointment</Nav.Link>
            //                         <Nav.Link href='/contact'>Contacts</Nav.Link>
            //                     </Nav>
            //                 </Col>
            //             </Row>
            //         </Container>
            //     </Navbar>
            // </Navbar>
        )
    }
};