import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Row, Col, Container, ListGroup, Card } from 'react-bootstrap';
import './FooterComponent.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export class FooterComponent extends React.Component {
    render() {
        return (
            <footer>
                <Container>
                    <Row>
                        <Col>
                            <h3>Harlie Nails</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </Col>
                        <Col>
                            <h6>Products</h6>
                            <ul>
                                <li>
                                    <a href = ''>link1</a>
                                </li>
                                <li>
                                    <a href = '' >link2</a>
                                </li>
                                <li>
                                    <a href = '' >link3</a>
                                </li>
                                <li>
                                    <a href = '' >link4</a>
                                </li>
                            </ul>
                        </Col>
                        <Col>
                            <h6>Services</h6>
                            <ul>
                                <li>
                                    <a href = ''>link1</a>
                                </li>
                                <li>
                                    <a href = '' >link2</a>
                                </li>
                                <li>
                                    <a href = '' >link3</a>
                                </li>
                                <li>
                                    <a href = '' >link4</a>
                                </li>
                            </ul>
                        </Col>
                    
                    </Row>
                    <hr className = 'm-0'/>
                    
                    <Row>
                        <Col>
                            <div className = 'socialIcons'>
                                <a className = 'facebook' href="#"><FontAwesomeIcon icon = {['fab', 'facebook']}/></a>
                                <a className = 'twitter' href="#"><FontAwesomeIcon icon = {['fab', 'twitter']}/></a>
                                <a className = 'instagram' href="#"><FontAwesomeIcon icon = {['fab', 'instagram']}/></a>
                                <a className = 'linkedin' href="#"><FontAwesomeIcon icon = {['fab', 'linkedin']}/></a>
                                <a className = 'skype' href="#"><FontAwesomeIcon icon = {['fab', 'skype']}/></a>
                            </div>
                        </Col>
                    </Row>
                </Container>

                <Container fluid style={{ paddingLeft: 0, paddingRight: 0 }}>
                    <Row noGutters = {true}>
                        <Col>
                            <div class="footer-copyright text-center py-3">© 2020 Copyright:
                                <a href="https://mdbootstrap.com/"> Harlie Nails</a>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </footer>
        );
    }
};