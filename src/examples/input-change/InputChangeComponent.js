import React from 'react';
export class InputChangeComponent extends React.Component {
  constructor(props) {
    super(props);
    // set initial state
    this.state = { userInput: '' };
    // bind handleChange so we can use it
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({
      userInput: e.target.value
    });
  }

  render() {
    return (
        <div>
            <input 
                onChange={this.handleChange} 
                type="text" />
            <h1>{this.state.userInput}</h1>
        </div>
    );
  }
}