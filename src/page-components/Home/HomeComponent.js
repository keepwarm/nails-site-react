import React from 'react';
import { BannerComponent } from '../../global-components/BannerComponent/BannerComponent';
import { Row, Container, Col, Form, Card, Image, CardGroup } from 'react-bootstrap';
import { StaffCardGroupComponent} from '../../global-components/StaffCardGroupComponent/StaffCardGroupComponent';
import { ServicesComponent } from '../../global-components/ServicesComponent/ServicesComponent';
import { AdvicesComponent } from '../../global-components/UsingFreeAPI/AdvicesComponent/AdvicesComponent';
import { QuotesComponent } from '../../global-components/UsingFreeAPI/QuotesComponent/QuotesComponent';
import { Coupons } from '../../global-components/Coupons/Coupons';
import { getServicesData } from '../../data/service-data';

export default class HomeComponent extends React.Component{
    constructor(props){
        super(props);
        this.createBannerData = this.createBannerData.bind(this);
    }

    createBannerData() {
        return [
            {
                imageUrl: 'https://tnailsspahouston.com/uploads/demo23uxpnnim/logo/2017/07/17/1_1500302242_95_33.jpg',
                title: 'Nails French',
                // description: 'Nails Art 1'
            },
            {
                imageUrl: 'https://queennailscharlottetown.com/uploads/fnail0afw5skl/logo/2019/07/11/s9.png',
                title: 'Ombre Design',
                // description: 'Nails Art 2'
            },
            {
                imageUrl: 'https://cknailsspa.com/uploads/fdemoicpswuke/logo/2018/06/14/2_1529029226_0_Slide%20(8).jpg',
                title: 'Hot Pink',
                // description: 'Nails Art 3'
            }
        ];

    }

    createStaffData(){
        return [
            {
                image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/the-witcher-yennefer-2-1582305242.png',
                name: 'Yennefer of Vengerberg',
                description: 'Yennefer was famed for her beauty. She was often described as having “raven black hair” that fell down her shoulders in “curls”, and piercing “violet eyes”. Words such as “ravishing” and “menacing” (“Sword of Destiny” by Andrzej Sapkowski) were used to present the sorceress.'
            },
            {
                image: 'https://cdn.vox-cdn.com/thumbor/5JNsoHzwS26-9SSpNACiQo8gdug=/722x585:3920x2785/1200x800/filters:focal(1241x976:2201x1936)/cdn.vox-cdn.com/uploads/chorus_image/image/65993826/TheWitcher_101_Unit_06900_RT.fk3ph4dhp.0.jpg',
                name: 'Geralt of Rivia',
                description: 'Geralt of Rivia is portrayed as a taciturn, cunning, and mercenary protagonist. He rarely partakes in the joys of friendship and human company (save his closest friends like Dandelion), although he has been known to enjoy women (such as Yennefer, his love) or a good tankard now and then.'
            },
            {
                image: 'https://www.geekgirlauthority.com/wp-content/uploads/2019/12/the-witcher-ciri-freya-allen-1200x640.png',
                name: 'Pricess Ciri',
                description: 'Cirilla Fiona Elen Riannon (better known as Ciri), was born in 1252 or 1253, and most likely during the Belleteyn holiday. She was the sole princess of Cintra, the daughter of Pavetta and Emhyr var Emreis (who was using the alias "Duny" at the time) as well as Queen Calanthe\'s granddaughter.'
            }
        ];
    };



    render(){
        return(
            <div>
                <BannerComponent bannerData={this.createBannerData()} />

                <Container>
                    <Row>
                        <Col>
                            <h1 className="text-center">-----Our Services-------</h1>
                            <CardGroup>
                                {getServicesData().map((data) => (
                                <ServicesComponent header={data.header} image={data.image} title={data.title} content={data.content}/>
                                 ))};
                             </CardGroup>
                             +
                        </Col>
                    </Row>
                </Container>
                <br />

                <Container>
                    <Row>
                        <Col>
                            <h1 style = {{textAlign: 'center'}}>-------Our Products------</h1>
                        </Col>
                    </Row>
                </Container>
                <br />

                <Container>
                    <Row>
                        <Col>
                            <h1 style = {{textAlign: 'center'}}>-------Coupons------</h1>
                            <Coupons />
                        </Col>
                    </Row>
                </Container>
                <br />

                <Container>
                    <Row>
                        <Col>
                            <h1 style = {{textAlign: 'center'}}>-------Harlie Nails Sample------</h1>
                        </Col>
                    </Row>
                </Container>
                <br />

                <Container>
                <Row>
                    <Col>
                        <h1 style = {{textAlign: 'center'}}>-------Our Staffs------</h1>
                        <StaffCardGroupComponent staffData = {this.createStaffData()}/>

                    </Col>
                </Row>
                </Container>
                <br />

                <Container>
                    <Card style = {{backgroundColor: 'lightpink', border: '1px dashed'}}>
                        <Row noGutters={true}>
                            <Col>                               
                                <Card.Title className="text-center" style = {{fontSize: '2.5em'}}>
                                    Have Questions? <br />
                                    <small style = {{fontSize: '0.5em'}}>Feel Free To Get In Touch!</small>
                                </Card.Title>
                                <Card.Body style = {{marginRight: '1em', marginLeft: '1em', marginBottom: '0', padding: '0'}}>
                                    <Form>
                                        <Form.Row>
                                            <Form.Group as = {Col} controlId = 'firstName'>
                                                <Form.Control type = 'text' placeholder = 'Your First Name' />
                                            </Form.Group>

                                            <Form.Group as = {Col} controlId = 'lastName'>
                                                <Form.Control type = 'text' placeholder = 'Your Last Name' />
                                            </Form.Group>
                                        </Form.Row>
                                        
                                        <Form.Group controlId='email'>
                                            <Form.Control type='email' placeholder='name@example.com' />
                                        </Form.Group>

                                        <Form.Group controlId='subject'>
                                            <Form.Control type='text' placeholder='subject' />
                                        </Form.Group>

                                        <Form.Group controlId = 'description'>
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control as = 'textarea' rows = '3' />
                                        </Form.Group>
                                    </Form>
                                </Card.Body>
                            </Col>
                            <Col>
                                <Image fluid src="https://www.thoughtco.com/thmb/ETpeAugLo3JvjZbD1Mn3fLqII08=/1885x1414/smart/filters:no_upscale()/lotus-flower-828457262-5c6334b646e0fb0001dcd75a.jpg"></Image>
                            </Col>
                        </Row>
                    </Card>
                </Container>
                <br />

                <Container>
                    <Row>
                        <Col>
                            <AdvicesComponent />
                        </Col>
                    </Row>
                </Container>
                <Container>
                    <Row>
                        <Col>
                            <QuotesComponent />
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }
}
