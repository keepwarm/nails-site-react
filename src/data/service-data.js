
export function getServicesData(){
    return [
        {
            header: 'Manicure',
            image: 'https://cdn1.treatwell.net/images/view/v2.i784919.w1472.h981.x19481829.jpg',
            title: 'Regular Pedicure',
            content: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.`
        },
        {
            header: 'Pedicure',
            image: 'https://image.shutterstock.com/image-photo/young-woman-getting-pedicure-beauty-260nw-1306859293.jpg',
            title: 'Special Pedicure',
            content: `It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
                It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
                and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
        },
        {
            header: 'OtherServices',
            image: 'https://www.headlinersinc.com/wp-content/uploads/2018/01/banner-17.jpg',
            title: 'Polish Change',
            content: `The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, 
                as opposed to using 'Content here, content here', making it look like readable English.`
        }
    ];
}