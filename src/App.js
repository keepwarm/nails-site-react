import React from 'react';
import HomeComponent from './page-components/Home/HomeComponent';
import AboutComponent from './page-components/About/AboutComponent';
import Pedicure from './page-components/Pedicure/Pedicure';
import Manicure from './page-components/Manicure/Manicure';
import PolishChange from './page-components/PolishChange/PolishChange';
import OtherServices from './page-components/OtherServices/OtherServices';
import DipPowder from './page-components/DipPowder/DipPowder';
import GelPolish from './page-components/GelPolish/GelPolish';
import RegularPolish from './page-components/RegularPolish/RegularPolish';
import Appointment from './page-components/Appointment/Appointment';
import Contacts from './page-components/Contacts/Contacts';
import { NavigationComponent } from './layout-components/NavigationComponent/NavigationComponent';
import { FooterComponent } from './layout-components/FooterComponent/FooterComponent';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

import { library } from '@fortawesome/fontawesome-svg-core'
import { fab, faFacebook, faTwitter, faLinkedin, faInstagram, faSkype } from '@fortawesome/free-brands-svg-icons'
library.add(fab, faFacebook, faTwitter, faLinkedin, faInstagram, faSkype )

function App(){
  return (
    <Router>
      <div>
        <NavigationComponent />

        <div>
          <Route exact path= '/' component={HomeComponent} />
          <Route path= '/about' component={AboutComponent} />
          
        </div>
        {/* <Switch>
          <Route path = '/'>
            <HomeComponent />
          </Route>
          <Route path = '/about'>
            <AboutComponent />
          </Route>
          <Route path = '/ped'>
            <Pedicure />
          </Route>
          <Route path = '/mani'>
            <Manicure />
          </Route>
          <Route path = '/change'>
            <PolishChange />
          </Route>
          <Route path = '/other'>
            <OtherServices />
          </Route>
          <Route path = '/dip'>
            <DipPowder />
          </Route>
          <Route path = '/gel'>
            <GelPolish />
          </Route>
          <Route path = '/regular'>
            <RegularPolish />
          </Route>
          <Route path = '/appointment'>
            <Appointment />
          </Route>
          <Route path = '/contact'>
            <Contacts />
          </Route>
        </Switch> */}
        
        <FooterComponent />

      </div>
    </Router>
  );
};


export default App;
